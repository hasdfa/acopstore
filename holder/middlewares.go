package holder

import (
	"acopStore/services/http/middlewares"
	"bitbucket.org/hasdfa/bgen/pkg/middlewareUtils"
)

var (
	middlewareSource = []*middlewareUtils.MiddlewareSource{
		middlewares.Auth.WithKey("auth"),
	}
)

var DefinedMiddlewares = middlewareUtils.Map(middlewareSource)
