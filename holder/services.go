package holder

import (
	"acopStore/services/http"
	"acopStore/services/http/authService"
	"acopStore/services/http/marketService"
	"acopStore/services/http/marketService/productsService"
)

var (
	AuthService   = authService.New()
	MarketService = marketService.New(ProductsService)

	ProductsService = productsService.New()
)

var (
	DefaultRouter = &http.ServiceController{
		Auth:   AuthService,
		Market: MarketService,
	}
)
