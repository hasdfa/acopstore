package models

import (
	"gopkg.in/mgo.v2/bson"
)

type (
	User struct {
		Id       bson.ObjectId `bson:"_id"             json:"-"`
		Username string        `json:"username" validate:"required"`
		Email    string        `json:"email" validate:"email,required"`

		Name    string `json:"name" validate:"required"`
		Surname string `json:"surname" validate:"required"`

		EncodedPassword []byte `json:"-"`
	}
)
