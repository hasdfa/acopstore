package models

type Paging struct {
	Skip  int `query:"skip"`
	Limit int `query:"limit"`

	Query string `query:"q"`
}
