package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

const (
	tokenLifetime = time.Hour * 24
)

type LoginRequest struct {
	Email    string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
	Device   string `json:"device" validate:"required"`
}

type RegisterRequest struct {
	Username string `json:"username" validate:"required"`
	Email    string `json:"email" validate:"email,required"`

	Name    string `json:"name" validate:"required"`
	Surname string `json:"surname" validate:"required"`

	SecretCode string `json:"email" validator:"required,email"`
	Password   string `json:"password" validate:"required"`
}

type UserToken struct {
	Id        bson.ObjectId `bson:"_id"    json:"-"`
	Owner     bson.ObjectId `json:"-"`
	Hash      string        `json:"hash"`
	Device    string        `json:"-"`
	CreatedAt int64         `json:"-"`
}

func (u *UserToken) IsExpired() bool {
	return time.Now().After(
		time.Unix(u.CreatedAt, 0).Add(tokenLifetime),
	)
}
