package models

import "gopkg.in/mgo.v2/bson"

type ProductRef struct {
	Uid string `query:"uid"`
}

type ProductRequest struct {
	Uid         string  `json:"uid,omitempty"`
	Title       string  `json:"title,omitempty"`
	Category    string  `json:"category,omitempty"`
	Description string  `json:"description,omitempty"`
	Price       float32 `json:"price,omitempty"`

	Image *RequestFile `json:"image,omitempty"`
}

type Product struct {
	Id  bson.ObjectId `bson:"_id"  json:"-"`
	Uid string        `json:"uid"`

	Title       string  `json:"title"`
	Category    string  `json:"category"`
	Description string  `json:"description"`
	ImageUid    string  `json:"image"`
	Price       float32 `json:"price"`
}
