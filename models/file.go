package models

import (
	"acopStore/database"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"github.com/xtgo/uuid"
)

type (
	RequestFile struct {
		Filename    string `json:"filename"`
		Data        []byte `json:"data"`
		ContentType string `json:"contentType"`
	}
)

var SupportContentTypes = map[string]string{
	"image/gif":                ".gif",
	"image/jpeg":               ".jpg",
	"image/pjpeg":              ".jpg",
	"image/png":                ".png",
	"image/svg+xml":            ".svg",
	"image/tiff":               ".tiif",
	"image/vnd.microsoft.icon": ".ico",
	"image/vnd.wap.wbmp":       ".wbmp",
	"image/webp":               ".webp",
}

func NewFile(uid string, data []byte, ext string) *RequestFile {
	return &RequestFile{
		Filename:    uid + uuid.NewTime().String(),
		ContentType: ext,
		Data:        data,
	}
}

func (f *RequestFile) FullName() string {
	if f.Filename == "" {
		f.Filename = uuid.NewRandom().String()
	}
	return f.Filename + f.ext()
}

func (f *RequestFile) ext() string {
	if ext, ok := SupportContentTypes[f.ContentType]; ok {
		return "." + ext
	}
	return ""
}

func (f *RequestFile) Save(db dbBase.Database) (err error) {
	file, err := database.MongoImages(db).Create(f.FullName())
	if err != nil {
		return
	}
	defer func() {
		err = file.Close()
		if err != nil {
			return
		}
	}()

	_, err = file.Write(f.Data)
	if err != nil {
		return
	}

	return
}
