package http

import (
	"acopStore/services/http/authService"
	"acopStore/services/http/marketService"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
)

type ServiceController struct {
	Auth   authService.IService
	Market marketService.IService
}

func (c *ServiceController) Router() *serviceUtils.ServiceConfiguration {
	return &serviceUtils.ServiceConfiguration{
		Children: map[string]serviceUtils.ConfigurableService{
			"/auth":   c.Auth,
			"/market": c.Market,
		},
	}
}
