package middlewares

import (
	"acopStore/database"
	"acopStore/models"
	"acopStore/translations"
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"bitbucket.org/hasdfa/bgen/pkg/httpUtils"
	"bitbucket.org/hasdfa/bgen/pkg/middlewareUtils"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"net/http"
)

const (
	authMiddlewareDescription = `This middleware using 'Authorization' header with token to authenticate user`
)

var (
	Auth = middlewareUtils.MiddlewareOf(
		authMiddleware,
	).WithDescription(authMiddlewareDescription)
)

func authMiddleware(context bgen.Context, _ map[string]string, next func() serviceUtils.Error) func() serviceUtils.Error {
	return func() serviceUtils.Error {

		return next()
		token := context.GetHeader(httpUtils.HeaderAuthorization)
		if token == "" {
			return serviceUtils.NewServiceFullTextError(
				translations.TokenRequired, http.StatusUnauthorized,
			)
		}
		repo := context.Repository().Default()

		var userToken models.UserToken
		err := database.Auth.Tokens(repo).Find(dbBase.M{
			"hash": token,
		}).One(&userToken)
		if err != nil {
			return serviceUtils.NewServiceBRTextError(translations.TokenNotFound)
		}

		if userToken.IsExpired() {
			return serviceUtils.NewServiceBRTextError(translations.TokenExpired)
		}

		var u models.User
		err = database.Users.User(repo).Find(
			dbBase.M{"_id": userToken.Owner},
		).One(&u)

		context.SetAuthenticated(&u)

		return next()
	}
}
