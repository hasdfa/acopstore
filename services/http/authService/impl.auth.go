package authService

import (
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
)

type service struct {
}

func New() IService {
	return &service{}
}

func (s *service) Router() *serviceUtils.ServiceConfiguration {
	return &serviceUtils.ServiceConfiguration{
		POST: serviceUtils.Definition{
			"/login":    s.Login,
			"/register": s.Register,
		},
	}
}
