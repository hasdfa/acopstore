package authService

import (
	"acopStore/database"
	"acopStore/models"
	"acopStore/translations"
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"golang.org/x/crypto/bcrypt"
)

func (s *service) Register(ctx bgen.Context, request *models.RegisterRequest) (*serviceUtils.Message, serviceUtils.Error) {
	repo := ctx.Repository().Default()

	// find already registered users
	n, err := database.Users.User(repo).Find(
		dbBase.M{"$or": []dbBase.M{
			{"email": request.Email},
			{"username": request.Username},
		}},
	).Count()
	if err != nil {
		return nil, serviceUtils.NewServiceISEError(err)
	}
	if n > 0 {
		return nil, serviceUtils.NewServiceBRTextError(
			translations.Shared.Translate(translations.UserAlreadyExists),
		)
	}

	// create new user
	u := &models.User{
		Username: request.Username,
		Email:    request.Email,
		Name:     request.Name,
		Surname:  request.Surname,
	}

	// generate password
	u.EncodedPassword, err = bcrypt.GenerateFromPassword([]byte(request.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, serviceUtils.NewServiceISEError(err)
	}

	// save user in database
	if err = database.Users.User(repo).Insert(u); err != nil {
		return nil, serviceUtils.NewServiceISEError(err)
	}

	return serviceUtils.MessageSuccess, nil
}
