package authService

import (
	"acopStore/database"
	"acopStore/models"
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"crypto/sha256"
	"encoding/base64"
	"github.com/labstack/gommon/random"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
	"time"
)

func (s *service) Login(ctx bgen.Context, request *models.LoginRequest) (*models.UserToken, serviceUtils.Error) {
	repo := ctx.Repository().Default()

	// find user
	var u models.User
	err := database.Users.User(repo).Find(
		dbBase.M{"email": request.Email},
	).One(&u)
	if err != nil {
		if err.Error() == "not found" {
			return nil, serviceUtils.NewServiceNFError(err)
		}
		return nil, serviceUtils.NewServiceISEError(err)
	}

	// check password
	err = bcrypt.CompareHashAndPassword(u.EncodedPassword, []byte(request.Password))
	if err != nil {
		return nil, serviceUtils.NewServiceBRTextError("password is invalid")
	}

	// remove all other token
	err = database.Auth.Tokens(repo).Remove(
		dbBase.M{"$or": []dbBase.M{
			{"owner": u.Id},
			{"device": request.Device},
		}},
	)

	// generate and save new token
	token := &models.UserToken{
		Id:        bson.NewObjectId(),
		Owner:     u.Id,
		CreatedAt: time.Now().Unix(),
		Hash:      generateHash(request, &u),
		Device:    request.Device,
	}
	err = database.Auth.Tokens(repo).Insert(token)
	if err != nil {
		return nil, serviceUtils.NewServiceISEError(err)
	}

	return token, nil
}

func generateHash(request *models.LoginRequest, u *models.User) string {
	hash := sha256.Sum256([]byte(
		request.Email +
			request.Device +
			request.Password +
			u.Name + u.Surname +
			time.Now().String() +
			random.New().String(15),
	))
	return base64.StdEncoding.EncodeToString(hash[:])
}
