package authService

import (
	"acopStore/models"
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
)

type IService interface {
	Login(ctx bgen.Context, request *models.LoginRequest) (*models.UserToken, serviceUtils.Error)
	Register(ctx bgen.Context, request *models.RegisterRequest) (*serviceUtils.Message, serviceUtils.Error)

	/* implement ConfigurableService */
	Router() *serviceUtils.ServiceConfiguration
}
