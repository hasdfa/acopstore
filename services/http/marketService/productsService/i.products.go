package productsService

import (
	"acopStore/models"
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
)

type IService interface {
	// Admin
	Add(ctx bgen.Context, product *models.ProductRequest) (*serviceUtils.Message, serviceUtils.Error)
	Change(ctx bgen.Context, request map[string]interface{}) (*serviceUtils.Message, serviceUtils.Error)
	Delete(ctx bgen.Context, ref *models.ProductRef) (*serviceUtils.Message, serviceUtils.Error)

	// Common
	List(ctx bgen.Context, paging *models.Paging) (interface{}, serviceUtils.Error)
	Info(ctx bgen.Context, ref *models.ProductRef) (*models.Product, serviceUtils.Error)

	/* implement ConfigurableService */
	Router() *serviceUtils.ServiceConfiguration
}
