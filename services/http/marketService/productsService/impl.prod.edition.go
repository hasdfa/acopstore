package productsService

import (
	"acopStore/database"
	"acopStore/models"
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"fmt"
	"github.com/google/uuid"
	"gopkg.in/mgo.v2/bson"
	"strconv"
)

func (s *service) Add(ctx bgen.Context, request *models.ProductRequest) (*serviceUtils.Message, serviceUtils.Error) {
	err := request.Image.Save(ctx.Repository().Default())
	if err != nil {
		return nil, serviceUtils.NewServiceDatabaseError(err)
	}

	p := &models.Product{
		Id:  bson.NewObjectId(),
		Uid: uuid.New().String(),

		Title:       request.Title,
		Category:    request.Category,
		Description: request.Description,
		Price:       request.Price,
		ImageUid:    request.Image.Filename,
	}

	err = database.Market.Products(ctx.Repository().Default()).Insert(p)
	if err != nil {
		return nil, serviceUtils.NewServiceDatabaseError(err)
	}

	return serviceUtils.MessageSuccess, nil
}

func (s *service) Change(ctx bgen.Context, request map[string]interface{}) (*serviceUtils.Message, serviceUtils.Error) {
	defaultSelector := dbBase.M{
		"uid": request["uid"],
	}

	var p models.Product
	err := database.Market.Products(ctx.Repository().Default()).Find(defaultSelector).One(&p)
	if err != nil {
		return nil, serviceUtils.NewServiceDatabaseError(err)
	}

	if title, ok := request["title"]; ok {
		p.Title = fmt.Sprint(title)
	}
	if desc, ok := request["description"]; ok {
		p.Description = fmt.Sprint(desc)
	}
	if category, ok := request["category"]; ok {
		p.Category = fmt.Sprint(category)
	}
	if price, ok := request["price"]; ok {
		pr, err := strconv.ParseFloat(fmt.Sprint(price), 32)
		if err == nil {
			p.Price = float32(pr)
		}
	}
	if image, ok := request["image"]; ok {
		if imageObj, ok := image.(map[string]interface{}); ok {
			fileObj := &models.RequestFile{}

			if data, ok := imageObj["data"]; ok {
				fileObj.Data = data.([]byte)
			}
			if ct, ok := imageObj["contentType"]; ok {
				fileObj.ContentType = fmt.Sprint(ct)
			}

			err := fileObj.Save(ctx.Repository().Default())
			if err != nil {
				return nil, serviceUtils.NewServiceDatabaseError(err)
			}
		}
	}

	err = database.Market.Products(ctx.Repository().Default()).
		Update(defaultSelector, p)
	if err != nil {
		return nil, serviceUtils.NewServiceDatabaseError(err)
	}

	return serviceUtils.MessageUpdated, nil
}

func (s *service) Delete(ctx bgen.Context, ref *models.ProductRef) (*serviceUtils.Message, serviceUtils.Error) {
	err := database.Market.Products(ctx.Repository().Default()).Remove(dbBase.M{
		"uid": ref.Uid,
	})
	if err != nil {
		return nil, serviceUtils.NewServiceDatabaseError(err)
	}

	return serviceUtils.MessageSuccess, nil
}
