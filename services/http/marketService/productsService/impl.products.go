package productsService

import (
	"acopStore/services/http/middlewares"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
)

type service struct {
}

func New() IService {
	return &service{}
}

func (s *service) Router() *serviceUtils.ServiceConfiguration {
	return &serviceUtils.ServiceConfiguration{
		GET: serviceUtils.Definition{
			"/list": s.List,
			"/item": s.Info,
		},

		PUT: serviceUtils.Definition{
			"/adm_item": &serviceUtils.MiddlewarePath{
				MiddlewareKey: middlewares.Auth.Key,
				Dest:          s.Add,
			},
		},
		POST: serviceUtils.Definition{
			"/adm_item": &serviceUtils.MiddlewarePath{
				MiddlewareKey: middlewares.Auth.Key,
				Dest:          s.Change,
			},
		},
		DELETE: serviceUtils.Definition{
			"/adm_item": &serviceUtils.MiddlewarePath{
				MiddlewareKey: middlewares.Auth.Key,
				Dest:          s.Delete,
			},
		},
	}
}
