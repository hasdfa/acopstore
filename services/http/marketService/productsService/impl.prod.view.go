package productsService

import (
	"acopStore/database"
	"acopStore/models"
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
)

func (s *service) List(ctx bgen.Context, params *models.Paging) (interface{}, serviceUtils.Error) {
	var p []*models.Product
	err := database.Market.Products(ctx.Repository().Default()).
		Find(nil).
		Skip(params.Skip).
		Limit(params.Limit).
		All(&p)
	if err != nil {
		return nil, serviceUtils.NewServiceDatabaseError(err)
	}

	return &p, nil
}

func (s *service) Info(ctx bgen.Context, ref *models.ProductRef) (*models.Product, serviceUtils.Error) {
	var p models.Product
	err := database.Market.Products(ctx.Repository().Default()).Find(dbBase.M{
		"uid": ref.Uid,
	}).One(&p)
	if err != nil {
		return nil, serviceUtils.NewServiceDatabaseError(err)
	}

	return &p, nil
}
