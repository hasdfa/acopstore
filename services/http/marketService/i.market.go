package marketService

import (
	"acopStore/services/http/marketService/productsService"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
)

type IService interface {
	Products() productsService.IService

	/* implement ConfigurableService */
	Router() *serviceUtils.ServiceConfiguration
}
