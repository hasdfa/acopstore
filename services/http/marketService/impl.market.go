package marketService

import (
	"acopStore/services/http/marketService/productsService"
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
)

type service struct {
	products productsService.IService
}

func New(s1 productsService.IService) IService {
	return &service{
		products: s1,
	}
}

func (s *service) Products() productsService.IService {
	return s.products
}

func (s *service) Router() *serviceUtils.ServiceConfiguration {
	return &serviceUtils.ServiceConfiguration{
		Children: serviceUtils.ChildrenDefinition{
			"/products": s.Products(),
		},
	}
}
