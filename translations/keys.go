package translations

const (
	UserAlreadyExists = "User already exists. Try to change username or email."

	TokenRequired = "Token header required"
	TokenNotFound = "Token was not found"
	TokenExpired  = "Token is expired"
)
