package translations

type Service interface {
	Translate(key string) string
}

var (
	Shared Service = &hardService{}
)

type hardService struct {
}

func (s *hardService) Translate(key string) string {
	return key
}
