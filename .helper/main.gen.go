package main

import (
	"acopStore/holder"
	"bitbucket.org/hasdfa/bgen/generator"
	"flag"
)

const (
	packageName = "acopStore"
)

var (
	startMode = flag.String("m", "gen", "Program run mode: gen|clear")
)

func init() {
	flag.Parse()
}

func main() {
	switch *startMode {
	case "gen":
		generator.Generate(
			packageName,
			holder.DefaultRouter,
			holder.DefinedMiddlewares,
		)
		break
	case "clear":
		generator.Clear(
			packageName,
		)
		break
	default:
		panic("invalid run mode: " + *startMode)
	}
}
