package main

import (
	_gen "acopStore/.gen"
	"acopStore/holder"
	"bitbucket.org/hasdfa/bgen"
	"bitbucket.org/hasdfa/bgen/pkg/configUtils"
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils"
	"bitbucket.org/hasdfa/bgen/pkg/serverUtils"
	"flag"
	"log"
)

var (
	configFile = flag.String("c", ".configs/local.json", "Configuration file path")
)

var (
	closing = make(chan error)

	SharedHttpServer = serverUtils.NewIrisServer()
)

func init() {
	flag.Parse()
}

func main() {
	config, err := configUtils.ParseFile(configFile)
	if err != nil {
		log.Fatal(err)
	}
	if !config.Has() {
		log.Fatal("no such configuration provided")
	}

	if config.HasDatabase() {
		bgen.SharedRepositories = dbUtils.NewDatabaseRepository(
			config.Repositories,
		)
	}
	if config.HasHttp() {
		runHttpServers(SharedHttpServer, config.Http)
	}

	if err := <-closing; err != nil {
		log.Fatal(err)
	}
}

func runHttpServers(server *serverUtils.IrisServer, config *configUtils.HttpConfig) {
	server.DefinedMiddlewares = holder.DefinedMiddlewares
	server.Routes = holder.DefaultRouter
	server.Initializer = _gen.InitIrisRouter
	server.InitRouter()

	for _, p := range config.Ports {
		go func(port *configUtils.ServerPort) {
			closing <- server.Start(port)
		}(p)
	}
}
