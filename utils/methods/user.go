package methods

import (
	"bitbucket.org/hasdfa/bgen/pkg/serviceUtils"
	"github.com/xtgo/uuid"
	"gitlab.com/horecart/backend/models/auth"
	"gitlab.com/horecart/backend/models/user"
	"gopkg.in/mgo.v2/bson"
)

func NewUser(request *auth.RegisterRequest) *user.FullUser {
	return &user.FullUser{
		Id:        bson.NewObjectId(),
		UUID:      uuid.NewRandom().String(),
		Username:  request.Username,
		Name:      request.Name,
		Surname:   request.Surname,
		Email:     request.Email,
		Telephone: request.Telephone,
	}
}

func UpdateUserInfo(u *user.User, request *user.ChangePersonalInfo) serviceUtils.Error {
	return nil
}
