package methods

import (
	"encoding/base64"
	"github.com/labstack/gommon/random"
	"gitlab.com/horecart/backend/models/auth"
	"gitlab.com/horecart/backend/models/user"
	"golang.org/x/crypto/sha3"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const (
	TokenLifecycle = time.Minute * 30
)

func GenerateToken(request *auth.LoginRequest, u *user.User) *auth.UserToken {
	token := &auth.UserToken{
		Id:        bson.NewObjectId(),
		StartedAt: time.Now(),
		LiveCycle: TokenLifecycle,
		UserId:    u.Id,
		DeviceId:  request.Device.Id,
		Active:    true,
	}

	hasher := sha3.New512()
	hasher.Write([]byte(u.Email + request.Password + request.Device.Fingerprint + random.New().String(25, random.Alphanumeric)))
	token.Hash = base64.URLEncoding.EncodeToString(hasher.Sum(nil))

	//Refresh token
	token.RefreshToken = GenerateRefreshToken(token)

	return token
}

func GenerateRefreshToken(token *auth.UserToken) *auth.UserRefreshToken {
	rHasher := sha3.New512()
	rHasher.Write([]byte(token.Hash + token.StartedAt.String() + token.Id.String() + random.New().String(25, random.Alphanumeric)))

	sHasher := sha3.New512()
	sHasher.Write([]byte(token.Hash + base64.URLEncoding.EncodeToString(rHasher.Sum(nil))))
	return &auth.UserRefreshToken{
		Hash:      base64.URLEncoding.EncodeToString(rHasher.Sum(nil)),
		SSCode:    base64.URLEncoding.EncodeToString(sHasher.Sum(nil)),
		StartedAt: token.StartedAt.Add(token.LiveCycle),
		Active:    false,
	}
}
