package methods

import (
	"github.com/pborman/uuid"
	"gitlab.com/horecart/backend/models/auth"
	"gitlab.com/horecart/backend/models/group"
	"gitlab.com/horecart/backend/models/user"
	"gopkg.in/mgo.v2/bson"
)

func NewGroup(request *auth.RegisterRequest, u *user.FullUser) *group.LargeGroup {
	return &group.LargeGroup{
		Id:       bson.NewObjectId(),
		UUID:     uuid.NewRandom().String(),
		Name:     request.Organization,
		FullName: request.OrganizationFull,
		Type:     group.Type(request.BusinessType),
		Members: map[string]string{
			u.UUID: "admin",
		},
		NIP:       request.NIP,
		Locations: request.Locations,
	}
}
