package methods

import (
	"bitbucket.org/hasdfa/bgen"
	"gitlab.com/horecart/backend/models/user"
)

func GetAuthenticated(context bgen.Context) *user.User {
	auth := context.GetAuthenticated()
	if auth == nil {
		return nil
	}
	if u, ok := auth.(*user.User); ok {
		return u
	}
	return nil
}
