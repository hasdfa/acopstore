package database

import (
	"bitbucket.org/hasdfa/bgen/pkg/dbUtils/dbBase"
	"gopkg.in/mgo.v2"
)

type (
	Database   = dbBase.Database
	Collection = dbBase.Collection
)

type (
	authCollection   string
	usersCollection  string
	marketCollection string
)

var (
	Auth   = authCollection("auth")
	Users  = usersCollection("users")
	Market = marketCollection("market")

	emptyDB = dbBase.TempDb()
)

func MongoImages(db Database) *mgo.GridFS {
	if db == nil {
		db = emptyDB
	}
	return db.Mongo().GridFS("images")
}

// User
func (c usersCollection) User(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c))
}
func (c usersCollection) Security(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".security")
}
func (c usersCollection) Devices(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".devices")
}
func (c usersCollection) Locations(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".locations")
}

// Auth
func (c authCollection) OldPasswords(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".oldPasswords")
}
func (c authCollection) Tokens(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".loginTokens")
}
func (c authCollection) EmailVerificationTokens(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".emailVerificationTokens")
}

// Market
func (c marketCollection) SubmitOrders(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".submitOrders")
}
func (c marketCollection) Orders(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".orders")
}
func (c marketCollection) OrdersHistory(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".ordersHistory")
}
func (c marketCollection) ComplexOrders(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".complexOrders")
}
func (c marketCollection) Products(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".products")
}
func (c marketCollection) OrderTemplates(db Database) Collection {
	if db == nil {
		db = emptyDB
	}
	return db.Collection(string(c) + ".orderTemplates")
}
